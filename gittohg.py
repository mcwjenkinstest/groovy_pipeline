import os
import re
import subprocess
import shlex
import logging
import datetime
import sys

WORKSPACE = '/home/mcw/Jenkins/workspace/GittoHg/'
GIT_LOG = HG_LOG = {}
GIT_REPO_PATH = HG_REPO_PATH = ""

#MANAGED_REPOS = {"s1":["ssh://hth@helixteamhub.cloud/multicoreware/projects/x265/repositories/mercurial/x265_2", "git@bitbucket.org:multicoreware/x265_git.git"]}
MANAGED_REPOS = {"x265":["ssh://hg@bitbucket.org/multicoreware/x265", "git@bitbucket.org:multicoreware/x265_git.git"]}


def execute_cli(cmd, need_value=False):
    '''
    Executing clone/pull command in shell prompt
    '''
    logger.info(cmd)
    args = shlex.split(cmd)
    execute = subprocess.Popen(args, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    if need_value:
        return execute
    if execute:
        logger.info("STDOUT: %s", execute.stdout.read())
        logger.info("STDERR: %s", execute.stderr.read())


def ifpresent(folder):
    '''
    checks if a particular repository is already available
    '''
    if_present = subprocess.Popen(["ls", folder], cwd=WORKSPACE, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    err = if_present.stderr.read()
    if err:
        logger.info("%s not present", folder)
        return err


def clone_repo(repo, url, tool):
    '''
    clone a hg/git repository
    repo - name of the repository
    tool - git/hg
    destination - the folder in which the repository has to be cloned
    '''
    logger.info("******************%s CLONE********************", tool)
    logger.info("Repository is not present. Freshly cloning")
    cmd = tool+" clone "+url+" ."
    execute_cli(cmd)
    logger.info("%s Cloned %s" %(tool, repo))


def repo_pull(repo_path, tool):
    '''
    Pulling the recent changes of a repository and updating the revison \
            to latest
    '''
    logger.info("******************%s PULL********************", tool)
    logger.info("Repo is present . So pulling and updating")
    os.chdir(repo_path)
    if tool == 'hg':
        cmd = "hg pull"
        execute_cli(cmd)
        logger.info("%s pull Done", tool)
        cmd = "hg update"
        execute_cli(cmd)
        logger.info("%s update Done", tool)
    else:
        cmd = "git pull"
        execute_cli(cmd)
        logger.info("%s pull Done", tool)


def repo_ops(repo, repo_path, tool):
    '''
    Performs hg operations like hg clone/pull
    repo -  name of the repository
    '''
    if not ifpresent(repo_path):
        logger.info("Sub-folder %s present . Pull needed", repo_path)
        os.chdir(repo_path)
        repo_pull(repo_path, tool)
    else:
        os.mkdir(repo_path)
        os.chdir(repo_path)
        if tool == 'hg':
            url = MANAGED_REPOS[repo][0]
            clone_repo(repo, url, tool)
        else:
            url = MANAGED_REPOS[repo][1]
            clone_repo(repo, url, tool)


def get_hg_branches():
    '''
    Getting all the available branches of a repository
    '''
    logger.info("Getting all the available HG branches")
    cmd = "hg branches"
    hg_branch_cs = {}
    for branches in (execute_cli(cmd, True)).stdout.readlines():
        temp = re.split(' ', branches)
        if re.search('inactive', temp[-1]):
            hg_branch_cs[temp[0]] = re.split(':', temp[-2].strip())
        else:
            hg_branch_cs[temp[0]] = re.split(':', temp[-1].strip())
    logger.info("Branches %s" %(hg_branch_cs))
    return hg_branch_cs


def get_git_branches():
    '''
    Getting all the available branches of a repository
    '''
    logger.info("Getting all the available GIT branches")
    cmd = "git branch --all"
    git_branches = []
    for branches in (execute_cli(cmd, True)).stdout.readlines():
        temp = re.split('/', branches)
        for i in temp:
            if not len(temp) > 3:
                if re.search(r'remotes', i):
                    git_branches.append(temp[-1].strip())
    logger.info("Branches %s" %(git_branches))
    return git_branches


def strip(commits, tool):
    '''
    Getting the commit log decription for each of the commits
    commits - list of commit logs for a branch
    tool - either hg/git
    '''
    for branch, log in commits.iteritems():
        for clog in range(0, len(log)):
            if tool == 'hg':
                log[clog] = log[clog].strip()[43:]
            else:
                log[clog] = log[clog].strip()[41:]
    return commits


def get_commits(tool):
    '''
    Fetching the commit history of all branches
    tool - either hg/git
    '''
    commits = {}
    if tool == 'hg':
        branch_cs = get_hg_branches()
        for branch in branch_cs.keys():
            logger.info("Getting commits of %s branch" %(branch.upper()))
            cmd = "hg log -l 50 -b " +branch +" --template '{node} | {desc|strip|firstline}'"
            commits[branch] = execute_cli(cmd, True).stdout.readlines()
    else:
        branch_cs = get_git_branches()
        for branch in branch_cs:
            print "--------------------------------------------------------"
            cmd = "git checkout "+branch
            logger.info("Getting commits of %s branch" %(branch.upper()))
            execute_cli(cmd)
            cmd = "git log --pretty=oneline -50"
            commits[branch] = execute_cli(cmd, True).stdout.readlines()
    logger.info("%s commits"%(tool.upper()))
    for k,v in commits.iteritems():
        print "Branch:",k
        print "commits:",
        for i in v:
            print i.strip()
        print "---------------------------------------------"
    commit_strip = strip(commits, tool)
    return commit_strip


def find_patch_needed(ele, git_arr, low, high):
    '''
    Using binary search to check if the latest commits \
            in both the Repos matches 
    '''
    if high >= low:
        mid = low+(high-low/2)
        if git_arr[mid] == ele:
            return mid
        elif ele in git_arr[low:mid]:
            return find_patch_needed(ele, git_arr, low, mid-1)
        else:
            return find_patch_needed(ele, git_arr, mid+1, high)


def import_patches():
    '''
    Uses format-patch to get patches from GIT and applies to HG
    '''
    patch = {}
    for i in GIT_LOG.keys():
        if not (i == 'master') and i not in HG_LOG.keys():
            print i
            print "New branch created"
            sys.exit()
    for k, v in HG_LOG.items():
        if k == "default":
            val = find_patch_needed(HG_LOG[k][0], GIT_LOG["master"], 0, len(GIT_LOG["master"])-1)
            patch['master'] = val
        else:
            val = find_patch_needed(HG_LOG[k][0], GIT_LOG[k], 0, len(GIT_LOG[k])-1)
            patch[k] = val
    for k, v in patch.items():
        if v > 0:
            os.chdir(GIT_REPO_PATH)
            cmd = "git checkout "+k
            execute_cli(cmd)
            cmd = "git format-patch -"+str(v)
            listp = execute_cli(cmd, True)
            os.chdir(HG_REPO_PATH)
            for i in listp.stdout.readlines():
                k = "default" if k == "master" else k
                cmd = "hg checkout "+k
                execute_cli(cmd)
                cmd = "hg import "+GIT_REPO_PATH+"/"+i
                if execute_cli(cmd, True).stderr.readlines():
                    logger.info("Error in importing to HG %s", k.upper())
                else:
                    output = execute_cli("hg push", True)
                    if output.stderr.readlines():
                        logger.info("Error :%s", output.stderr.readlines())
                    else:
                        logger.info("PUSH successfull : \n %s", output.stdout.readlines())
                os.remove(GIT_REPO_PATH+"/"+i.strip())
        else:
            logger.info("Nothing to import for %s", k.upper())


def githg():
    '''
    Creating bookmarks for each of the available branches & \
            pushing it to GIT
    repo - repository name
    '''
    logger.info("*************GIT-HG migration starts*************")
    global GIT_LOG, HG_LOG
    for path in [HG_REPO_PATH, GIT_REPO_PATH]:
        os.chdir(path)
        if 'hg' in path:
            HG_LOG = get_commits('hg')
        else:
            GIT_LOG = get_commits('git')
    import_patches()


def check_repo(repo):
    '''
    Checks if the Repository folder is present
    repo - name of the repo
    '''
    global HG_REPO_PATH, GIT_REPO_PATH
    logger.info("Checking if the %s repository folder is present", repo)
    HG_REPO_PATH = WORKSPACE+repo+"/"+repo+"-hg"
    GIT_REPO_PATH = WORKSPACE+repo+"/"+repo+"-git"
    if ifpresent(repo):
        logger.info("Main folder %s not present.Creating one", REPO_NAME)
        os.mkdir(WORKSPACE+repo)
        os.chdir(WORKSPACE+repo)
        repo_ops(repo, HG_REPO_PATH, 'hg')
        repo_ops(repo, GIT_REPO_PATH, 'git')
        githg()
    else:
        repo_ops(repo, HG_REPO_PATH, 'hg')
        repo_ops(repo, GIT_REPO_PATH, 'git')
        githg()


if __name__ == "__main__":
    # REPO_NAME=str(raw_input("Repo:"))
    REPO_NAME = str(sys.argv[1])
    print REPO_NAME+" GIT TO HG MIGRATION "
    FILENAME = WORKSPACE+"githg"+REPO_NAME+"-"+datetime.datetime.now().strftime('%d-%m-%Y-%H%M%S')
    log_format = '%(asctime)s %(levelname)s - %(funcName)s: %(message)s'
    logging.basicConfig(filename=FILENAME+".log",
            format=log_format,
            filemode='w')
    logger=logging.getLogger()
    logger.setLevel(logging.DEBUG)
    f = logging.FileHandler(FILENAME+".log")
    logger.addHandler(f)
    s = logging.StreamHandler()
    logger.addHandler(s)
    check_repo(REPO_NAME)
