import os
import sys
import getopt, sys
import platform
import subprocess as sub
from subprocess import Popen, PIPE
import time

test_harness_folder  = os.path.dirname(os.path.abspath('__file__'))

try:
    from conf import my_source
    my_source = os.path.expanduser(my_source)
except ImportError, e:
    print e
    sys.exit(1)

try:
    from conf import encoder_binary_name
except ImportError, e:
    encoder_binary_name = 'x265'

try:
    from conf import version_control
except ImportError, e:
    version_control = 'hg'
hg = True if version_control == 'hg' else False
    
def pull_source_code():
        ret=sub.Popen("%s pull" %(version_control), shell=True, cwd=my_source)
        if ret.wait()!=0:
            print("Pull failed for source!\n")
            sys.exit(0)

def get_tag():
    if hg:
        out, err = Popen(['hg', 'tags'], stdout=PIPE, stderr=PIPE, cwd=my_source).communicate()
        if err:
            raise Exception('Unable to determine hg tags: ' + err)
    return out

def get_branch():
    if hg:
        out, err = Popen(['hg', 'branches'], stdout=PIPE, stderr=PIPE, cwd=my_source).communicate()
        if err:
            raise Exception('Unable to determine hg branch: ' + err)
    return out

def get_commit(user_branch_name):
    if hg:
        tags = get_tag()
        tags = tags.split('\n')
        tag_commit = tags[1].split(':')[1].split(' ')[0]

        branches = get_branch()
        branches = branches.split('\n')
        branches = [i for i in branches if i != '']
        final_branch = []
        for i in range(len(branches)):
            final_branch.append(branches[i].split(':')[0].split(' ')[0])
        if (user_branch_name != 'tag') and final_branch.count(user_branch_name) == 0:
            print "The repo only has %s branch!" %(final_branch)
            print "\nCan not find %s branch that specific, please check it again!" %user_branch_name
            sys.exit(0)
        if user_branch_name == 'tag':
            latest_commit = tag_commit
        elif user_branch_name == 'stable':
            for i in range(len(branches)):
                if branches[i].split(' ')[0] == 'stable':
                    latest_commit = branches[i].split(':')[1].split(' ')[0]
        elif user_branch_name == 'default':
            for i in range(len(branches)):
                if branches[i].split(' ')[0] == 'default':
                    latest_commit = branches[i].split(':')[1].split(' ')[0]
        else:
            for i in range(len(branches)):
                if branches[i].split(' ')[0] == user_branch_name:
                    latest_commit = branches[i].split(':')[1].split(' ')[0]
    else:
        check_out, check_err = Popen(['git', 'checkout', user_branch_name], stdout=PIPE, stderr=PIPE, cwd=my_source).communicate()
        if "Already on 'master'" in check_err:
            print ' '
        out, err = Popen(['git', 'rev-parse', 'HEAD'], stdout=PIPE, stderr=PIPE, cwd=my_source).communicate()
        if err:
            raise Exception('Unable to determine source version: ' + err)
        latest_commit = out.strip('\n')
    return latest_commit

def update_latest_commit(user_branch_name, commit):
    if hg:
        clean_changes=sub.Popen('hg update --clean', cwd=my_source, shell=True)
        if clean_changes.wait()!= 0:
           print("failed to clean uncommitted changes")
           
        ret=sub.Popen('hg update -r '+ commit, cwd=my_source, shell=True)
        if ret.wait()!= 0:
           print("failed to update code to latest commit")
           sys.exit(0)
        else:
           print("\nUpdated the code to commit: %s" %commit)

def pull_testharness():
        ret=sub.Popen("hg pull", shell=True, cwd=test_harness_folder)
        #print ret
        if ret.wait()!=0:
            print("Pull failed x265 repo")

def parseParameter():
    
    longopts = ['help', 'branch=', 'test=']
    opts, args = getopt.getopt(sys.argv[1:], 'hb:t:', longopts)
    if opts == []:
        #print opts[0]
        print "\nPlease use branch-test.py -b BranchName -t smoke-test to run test..."
        sys.exit(0)
    else:
        for opt_name, opt_value in opts:
            #print opt_name, opt_value
            # for example: ./branch-test.py -r releasel
            if opt_name in ('-h', '--help'):
                print sys.argv[0], '                 [OPTIONS]\n'
                print '\t   -h/--help           show this help'
                print '\t   -b <string>         specify a branch to test (release / tag / stable / default)'
                print '\t   -t <string>         specify test type (smoke / regression / save-load)\
                                                                           the <string> can be any string for git repo(x264/ ffmpeg)!'
                sys.exit(0)
            if opt_name in ('-b', '--branch'):
                test_branch = opt_value
            if opt_name in ('-t', '--test'):
                if 'smoke' in opt_value:
                    test_type = "smoke-test.py"
                elif 'regression' in opt_value:
                    test_type = "regression-test.py"
                elif 'save-load' in opt_value:
                    test_type = "regression-test.py --save-load-test"
                else:
                    print "\nWrong test type, please input string include 'smoke' or 'regression' or 'save-load' after '-t'!"
                    sys.exit(1)
        #print test_branch, test_type
        return test_branch, test_type

def runtest(branchname, recent_commit, latest_commit, test_type):
    job_result = 'job_result_%s_%s_%s.txt' %(encoder_binary_name, branchname, test_type)
    print("(recent %s %s : %s)" %(encoder_binary_name, branchname, recent_commit))
    print("(latest %s %s : %s)" %(encoder_binary_name, branchname, latest_commit))
    out, err = Popen(['python', test_type], stdout=PIPE, stderr=PIPE, cwd=test_harness_folder).communicate()
    print err
    print out
    '''
    if recent_commit == latest_commit:
        if os.path.exists(job_result):
            last_job_result = open(job_result, 'r')
            if 'Fail' in last_job_result:
                raise Exception('No new commit, but the job result should keep same as last build result: ' + 'Fail')
                sys.exit(1)
            else:
                print "\nNo new commit on %s %s!" %(encoder_binary_name, branchname)
        else:
            print "No new commit, bu can not find the file %s, so keep the job result SUCCESS first time!!!" %(job_result)
    else:
        update_latest_commit(branchname, latest_commit)
        files = open(job_result, 'w')
        out, err = Popen(['python', test_type], stdout=PIPE, stderr=PIPE, cwd=test_harness_folder).communicate()
        if err:         
            files.write('Fail')
            files.close()
            raise Exception('The test failed: ' + err)
            sys.exit(1)
        else:
            print out
            parse_out = out.split('\n')
            for line in range(len(parse_out)):
                if 'Errors written' in parse_out[line]:
                    files.write('Fail')
                    files.close()
                    raise Exception('The test failed: ' + parse_out[line])
                    sys.exit(1)
            files.write('Success')
            files.close()
            print 'The %s %s for %s successfully!\n' %(encoder_binary_name, test_type, branchname)
       '''             
        
if __name__ == '__main__':
    
    #get branch name
    test_branch, test_type = parseParameter()

    if not hg:
        test_branch = 'master'

    #get recent commit
    recent_commit = get_commit(test_branch)

    #pull source code
    pull_source_code()

    #get latest commit
    latest_commit = get_commit(test_branch)
    
    #pull test-harness
    pull_testharness()
    
    #run test
    runtest(test_branch, recent_commit, latest_commit, test_type)
