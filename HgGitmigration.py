import os
import re
import subprocess
import shlex
import time
import logging
import datetime
import sys

WORKSPACE=r"/home/mcw/Jenkins/workspace/Hg-Git/"
managed_repos={"test-harness":["ssh://hg@bitbucket.org/multicoreware/test-harness","git@bitbucket.org:multicoreware/testharness_git.git"],\
               "x265":["ssh://hg@bitbucket.org/multicoreware/x265","git@bitbucket.org:multicoreware/x265_git.git"],"uhd":["",""]}

TRIAL=0
#hg_repo_path=r"/tmp/testing/test-harness/test-harness-hg/"

def ifpresent(folder):
    '''
    checks if a particular repository is already available
    '''
    ifpresent = subprocess.Popen(["ls",folder] ,cwd=WORKSPACE,stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    err=ifpresent.stderr.read()
    if err:
        logger.info("%s not present",folder)
        return err

def execute_cli(cmd):
    '''
    Executing clone/pull command in shell prompt
    '''
    logger.info(cmd)
    args=shlex.split(cmd)
    execute=subprocess.Popen(args,stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    if "branch" in cmd:
        return execute
    if execute:
        logger.info("STDOUT: %s",execute.stdout.read())
        logger.info("STDERR: %s",execute.stderr.read())

def hg_bookmark(repo,current_branch,rev):
    '''
    Creating a reference for a branch
    repo - repository name
    current_branch - branch for which reference will be created
    '''
    logger.info("****************HG BOOKMARKS**************")
    logger.info("Creating bookmark for %s",current_branch)
    if current_branch=="default":
        cmd="hg bookmarks -f -r "+rev+" master"
    else :
        cmd="hg bookmarks -f -r "+rev+" "+current_branch
    execute_cli(cmd)
    logger.info("Bookmarked %s"%(current_branch))
    hg_push(repo,current_branch)
    delete_bookmarks(current_branch)


def delete_bookmarks(current_branch):
    '''
    Deleting the reference once the changes are pushed
    current_branch - branch for which reference will be deleted
    '''
    logger.info("Deleting bookmark for %s",current_branch)
    if current_branch=="default":
        cmd="hg bookmarks --delete master"
    else:
        cmd="hg bookmarks --delete "+current_branch
    execute_cli(cmd)

def get_hg_branches():
    '''
    Getting all the available branches of a repository
    '''
    logger.info("Getting all the available branches")
    cmd="hg branches"
    hg_branch_cs={}
    for branches in (execute_cli(cmd)).stdout.readlines():
        temp=re.split(' ',branches)
        if re.search('inactive',temp[-1]):
            hg_branch_cs[temp[0]]=re.split(':',temp[-2].strip())
        else :
            hg_branch_cs[temp[0]]=re.split(':',temp[-1].strip())
    logger.info("Branches %s"%(hg_branch_cs))
    return hg_branch_cs

def hggit(repo):
    '''
    Creating bookmarks for each of the available branches & \
            pushing it to GIT
    repo - repository name
    '''
    logger.info("*************HG-GIT migration starts*************")
    branch_cs=get_hg_branches()
    for branch in branch_cs.keys():
        rev_num=branch_cs[branch][1]
        hg_bookmark(repo,branch,rev_num)

def hg_push(repo,branch):
    '''
    Pushing changes to GIT repo
    repo - repository name
    '''
    logger.info("Pushing changes to GIT")
    cmd="hg push git+ssh://"+managed_repos[repo][1]
    #cmd="hg push "+managed_repos[repo][1]
    execute_cli(cmd)

def clone_hg_git(repo,url,tool):
    '''
    clone a hg/git repository
    repo - name of the repository
    tool - git/hg
    destination - the folder in which the repository has to be cloned
    '''
    logger.info("******************HG CLONE********************")
    logger.info("Repository is not present. Freshly cloning")
    cmd=tool+" clone "+url+" ."
    execute_cli(cmd)
    logger.info("%s Cloned %s"%(tool,repo))

def hg_pull(repo):
    '''
    Pulling the recent changes of a repository and updating the revison \
            to latest
    '''
    logger.info("******************HG PULL********************")
    logger.info("Repo is present . So pulling and updating")
    cmd="hg pull"
    execute_cli(cmd)
    logger.info("HG pull Done")
    cmd="hg update"
    execute_cli(cmd)
    logger.info("HG update Done")

def hg_ops(repo,hg_repo_path):
    '''
    Performs hg operations like hg clone/pull
    repo -  name of the repository
    '''
    if not ifpresent(hg_repo_path):
        logger.info("Sub-folder %s present . Pull needed",hg_folder)
        os.chdir(hg_repo_path)
        hg_pull(repo,hg_repo_path)
    else :
        os.mkdir(hg_repo_path)
        os.chdir(hg_repo_path)
        url=managed_repos[repo][0]
        clone_hg_git(repo,url,"hg")

def check_repo(repo):
    '''
    Checks if the Repository folder is present
    repo - name of the repo
    '''
    logger.info("Checking if the %s repository folder is present",repo)
    hg_repo_path=WORKSPACE+repo+"/"+repo+"-hg"
    if ifpresent(repo):
        logger.info("Main folder %s not present.Creating one",repo_name)
        os.mkdir(WORKSPACE+repo)
        os.chdir(WORKSPACE+repo)
        hg_ops(repo,hg_repo_path)
        hggit(repo)
    else :
        os.chdir(hg_repo_path)
        hg_pull(repo)
        hggit(repo)

if __name__== "__main__":
    #repo_name=str(raw_input("Repo:"))
    repo_name= str(sys.argv[1])
    print repo_name+" HG TO GIT MIGRATION "
    filename=WORKSPACE+repo_name+"-"+datetime.datetime.now().strftime('%d-%m-%Y-%H%M%S')
    log_format = '%(asctime)s %(levelname)s - %(funcName)s: %(message)s'
    logging.basicConfig(filename=filename+".log",
            format=log_format,
            filemode='w')
    logger=logging.getLogger()
    logger.setLevel(logging.DEBUG)
    f = logging.FileHandler(filename+".log")
    logger.addHandler(f)
    s = logging.StreamHandler()
    logger.addHandler(s)
    check_repo(repo_name)
