#import pdb
#pdb.set_trace()
import os
import subprocess
import shlex
import time
import logging
import datetime

WORKSPACE=r"/tmp/testing/"
managed_repos={"test-harness":["ssh://hg@bitbucket.org/jananite/hg_test","git@bitbucket.org:jananite/git123.git"],\
               "x265":["",""],"uhd":["",""]}

FE_SCRIPT=r"hg-fast-export.sh"
FE_PATH=WORKSPACE+r"/fast-export/"
TRIAL=0

def ifpresent(folder):
 '''
 checks if a particular repository is already available
 '''
 ifpresent = subprocess.Popen(["ls",folder] ,stdout=subprocess.PIPE, stderr=subprocess.PIPE)
 err=ifpresent.stderr.read()
 if err:
  logger.info(err)
 return err

def execute_cli(cmd):
 '''
 Executing clone/pull command in shell prompt
 '''
 args=shlex.split(cmd)
 execute=subprocess.Popen(args, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
 logger.info(execute.stdout.read())
 logger.info(execute.stderr.read())

def git_push(repo):
 #print os.getcwd()
 if TRIAL == 0: 
  execute_cli("git checkout HEAD")
  time.sleep(1)
  execute_cli("git remote add origin git@bitbucket.org:jananite/git123.git")
  time.sleep(1)
 else : 
  execute_cli("git checkout HEAD")
  time.sleep(1)
 execute_cli("git push -u origin master") 

def fast_export(repo):
 '''
 Getting the changes of a particular repo from hg and pushes to git
 '''
 git_repo=WORKSPACE+repo+"/"+repo+"-git/"
 hg_repo=WORKSPACE+repo+"/"+repo+"-hg/"
 os.chdir(git_repo)
 cmd=FE_PATH+FE_SCRIPT+" -r "+hg_repo
 execute_cli(cmd)
 git_push(repo)

def pull_git(repo):
 logger.info("Git pull")

def clone_hg_git(repo,url,tool,destination=None):
  '''
  clone a hg/git repository
  repo - name of the repository
  tool - git/hg
  destination - the folder in which the repository has to be cloned
  '''
  cmd=tool+" clone "+url+" "+(destination if destination else " ")
  execute_cli(cmd)
  logger.info("%s Cloned %s"%(tool,repo))
 
def hg_pull(repo):
 logger.info("HG pull")

def git_ops(repo):
 '''
 Performs git operations like git clone/pull
 repo -  name of the repository
 '''
 os.chdir(WORKSPACE)
 if repo=="fast-export" :
  if ifpresent(repo):
   logger.info("Fast-export not present")
   url="http://repo.or.cz/fast-export.git"
   clone_hg_git(repo,url,"git")
  else :
   logger.info("Fast-export present. Doing nothing")
 else :
   os.chdir(WORKSPACE+repo)
   git_folder=repo+"-git"
   if ifpresent(git_folder):
    TRIAL=1
    os.mkdir(git_folder)
    os.chdir(git_folder)
    cmd="git init"
    execute_cli(cmd)
    logger.info("GIT init %s git",repo)
 
def hg_ops(repo):
 '''
 Performs hg operations like hg clone/pull
 repo -  name of the repository
 '''
 os.chdir(WORKSPACE+repo)
 hg_folder=repo+"-hg"
 if not ifpresent(hg_folder):
  logger.info("Sub-folder present. Pull needed")
 else :
  url=managed_repos[repo][0]
  clone_hg_git(repo,url,"hg",hg_folder)

def check_repo(repo):
  '''
     Checks if the Repository folder is present
     repo - name of the repo
     tool - either hg/git
  '''
  os.chdir(WORKSPACE)
  if repo=="fast-export":
   git_ops(repo)
  else: 
   if ifpresent(repo):
    logger.info("Main folder %s not present.Creating one",repo_name)
    os.mkdir(WORKSPACE+repo)
    hg_ops(repo)
    git_ops(repo)
    fast_export(repo)
   else :
    hg_pull(repo)
    fast_export(repo)
       
if __name__== "__main__":
 repo_name=str(raw_input("Repo:"))
 filename=WORKSPACE+repo_name+datetime.datetime.now().strftime('%d-%m-%Y-%H%M%S')
 log_format = '%(asctime)s %(levelname)s - %(funcName)s: %(message)s'
 logging.basicConfig(filename=filename+".log",
                    format=log_format, 
                    filemode='w')
 logger=logging.getLogger()
 logger.setLevel(logging.DEBUG)
 check_repo("fast-export")
 check_repo(repo_name)
