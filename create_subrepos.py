import os
import re
import subprocess
import shlex
import sys
import urllib

UHD = os.getcwd()
X264_PATH = os.path.join(UHD,'x264')
X265_PATH = os.path.join(UHD,'x265')
username = pwd = ""

from version_list import versions

def execute_cli(cmd, path, need_value=False):
    '''
    Executing clone/pull command in shell prompt
    '''
    args1 = shlex.split(cmd)
    execute = subprocess.Popen(args1,cwd = path ,stdout=subprocess.PIPE,stderr=subprocess.PIPE)
    tmp=execute.communicate()
    output=[]
    for lines in tmp[0].split('\n'):
        output.append(lines.strip())
    if any ('failed' in i for i in tmp):
        print "Wrong credentials. Exiting"
        os.rmdir(repo)
        sys.exit()
    if need_value:
        return output

def branch_inuse():
    git_branches=get_git_branches()
    cmd = "git branch"
    for branch in execute_cli(cmd,UHD,True) :
        if branch.startswith('*') :
            br=branch.strip('* ').strip('\n')
            if br in git_branches:
                print "Current UHDKit branch is "+br
                return br
            else :
                print "Local branch in use . Exiting"
                sys.exit()

def remove_pwd():
    for PATH in X264_PATH,X265_PATH:
        file_name = os.path.join(PATH,'.git','config')
        with open (file_name,'r') as f:
            data=f.readlines()
            f.close()
        for i in range(0,len(data)):
            if 'url' in data[i] :
                if 'x264' in PATH :
                    data[i] = "\turl = https://bitbucket.org/multicoreware/x264-uhdkit.git\n"
                else :
                    data[i] = "\turl = https://bitbucket.org/multicoreware/x265-git.git\n"
        with open (file_name,'w') as f:
            f.writelines(data)

def get_git_branches():
    '''
    Getting all the available branches of a repository
    '''
    git_branches=[]
    cmd = "git branch --all"
    for branches in (execute_cli(cmd, UHD,True)):
        temp = re.split('/', branches)
        for i in temp:
            if len(temp)==3 :
                if re.search(r'remotes', i):
                    git_branches.append(temp[-1].strip())
    return git_branches

def subrepo(branch,repo):
    print "Checking out "+repo+" version"
    cmd="git checkout "+versions[branch][repo]
    if repo == 'x264' :
        execute_cli(cmd,X264_PATH)
    else : 
        execute_cli(cmd,X265_PATH)

def checking_sub_version(branch,repo):
    print "Checking sub versions of "+repo
    cmd="git rev-parse HEAD"
    if repo == 'x264' :
        commitid=execute_cli(cmd,X264_PATH,True)
    else :
        commitid=execute_cli(cmd,X265_PATH,True)
    if commitid[0].strip()== versions[branch][repo] :
        pass
    else :
        subrepo(branch,repo)

def clone_repo(repo):
    '''
    clone a hg/git repository
    repo - name of the repository
    tool - git/hg
    destination - the folder in which the repository has to be cloned
    '''
    global username,pwd,k
    env_var=os.environ.copy()
    if not (username and pwd):
        if 'BB_UNAME' in env_var.keys() and 'BB_PWD' in env_var.keys():
            username=urllib.quote(env_var['BB_UNAME'])
            pwd=urllib.quote(env_var['BB_PWD'])
            x264_url="https://"+username+":"+pwd+"@"+re.split('https://',REPO_URL['x264'])[1]
            x265_url="https://"+username+":"+pwd+"@"+re.split('https://',REPO_URL['x265'])[1]
            REPO_URL['x265']=x265_url
            REPO_URL['x264']=x264_url
        else :
            print "Set BB_UNAME and BB_PWD in environmental variables for cloning private repos"
            os.rmdir(repo)
            sys.exit()
    url=REPO_URL[repo]
    print "Cloning "+repo
    cmd="git clone "+url+" "+repo
    execute_cli(cmd,UHD)

def check_repo(repo,branch):
    '''
    Checks if the Repository folder is present
    repo - name of the repo
    '''
    if os.path.isdir(repo):
        checking_sub_version(branch,repo)
    else :
        os.mkdir(repo)
        clone_repo(repo)
        subrepo(branch,repo)

if __name__== "__main__":
    global REPO_URL
    branch=branch_inuse()
    REPO_URL = {"x265":"https://bitbucket.org/multicoreware/x265_git.git", "x264":"https://bitbucket.org/multicoreware/x264-uhdkit.git"}
    repo_list = ['x264','x265']
    for repo in repo_list:
        check_repo(repo,branch)
    remove_pwd()
