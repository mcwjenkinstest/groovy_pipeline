versions = { 'master' : {'x265':'d0e0bc820ff245cab8c71f09649dde976c610ddc','x264' :'a2a22d1a7ee8e1bcb6f37cecdf2d97ccf5c68ab8'},
             'stable' : {'x265':'d0e0bc820ff245cab8c71f09649dde976c610ddc','x264' :'a2a22d1a7ee8e1bcb6f37cecdf2d97ccf5c68ab8'},
             'release_v2.3' : {'x265':'fbffd2a968169fffa4937b9418698ec7420a8ddf','x264' :'0c3591cb8982016589943fd19921eebbc37d10e7'},
             'release_v2.4' : {'x265':'cc76c7762b6bca47121659190de3aecc221bda36','x264' :'6e910b609d2fa725a7e08df57ac10a01c3b134d7'},
             'release_v2.5' : {'x265':'d0e0bc820ff245cab8c71f09649dde976c610ddc','x264' :'a2a22d1a7ee8e1bcb6f37cecdf2d97ccf5c68ab8'}
           }
